(ns auth
  (:require  [clojure.test :as t]
             [django-clj.auth :as auth]))

(def passwords
  {:argon2
   [["test" "argon2$argon2i$v=19$m=512,t=2,p=2$RVgzcG9zb2hRMnBX$e3wM2bw6uTYhIK8DBRqDLg"]
    ["qwerty" "argon2$argon2i$v=19$m=512,t=2,p=2$NWxDNHVLWlhxTlVp$ot/8KpD7wEESyk5eJEcWfw"]
    ["django" "argon2$argon2i$v=19$m=512,t=2,p=2$SEpoQW5keG1YSG9s$gm3TNjyWqzEj+lWdcO5HBQ"]]})

(t/deftest argon2-verification
  (t/testing
   (t/is 
    (->> (:argon2 passwords)
         (map (fn [[p h]] (auth/django-verify-password h p)))
         (every? true?)))))

