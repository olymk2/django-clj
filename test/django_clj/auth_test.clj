(ns django-clj.auth-test
  (:require [django-clj.auth :as auth]
            [clojure.test :as t :refer [deftest testing is]]
            [toucan.db :as db]))

(def dummy-jwt-token "dummy-token")
(def unhashed-password "qwerty")
(def hashed-password "argon2$argon2i$v=19$m=512,t=2,p=2$NWxDNHVLWlhxTlVp$ot/8KpD7wEESyk5eJEcWfw")

(t/deftest django-user-auth-test []
  (t/testing "Test valid password returns true"
    (with-redefs [db/query (fn [_] [{:username "user" :password hashed-password}])]
      (t/is (map? (auth/django-user-auth "user" unhashed-password)))))

  (t/testing "Test invalid password returns true"
    (with-redefs [db/query (fn [_] [{:username "user" :password hashed-password}])]
      (t/is (= nil (auth/django-user-auth "user" "it's the wrong password"))))))


(deftest django-generate-token-test []
  (testing "Check we get a jwt string and can decode it on valid request"
    (with-redefs [db/query (fn [_] [{:username "user" :password hashed-password}])]
      (let [result (auth/django-generate-user-token dummy-jwt-token "user" unhashed-password)]
        (is (string? result))
        (is (map? (auth/django-jwt-unsign dummy-jwt-token result))))))

  (testing "No match"
    (with-redefs [db/query (fn [_] [{:username "user" :password hashed-password}])]
      (is (nil? (auth/django-generate-user-token dummy-jwt-token "user" "it's the wrong password"))))))


(deftest django-jwt-sign-unsign-test []
  (testing "Missing user_id or secret returns nil"
    (is (nil? (auth/django-jwt-sign dummy-jwt-token {})))
    (is (nil? (auth/django-jwt-sign dummy-jwt-token {:is_staff true})))
    (is (nil? (auth/django-jwt-sign nil {})))
    (is (nil? (auth/django-jwt-sign nil nil))))

  (testing "secret and user_id return jwt string"
    (let [result (auth/django-jwt-sign dummy-jwt-token {:id 1})] 
      (is (string? result))
      (is (= {:token_type "access", :user_id 1, :is_staff false, :is_superuser false}
             (auth/django-jwt-unsign dummy-jwt-token result))))))

