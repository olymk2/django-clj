(ns django-clj.drf
  (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]
            [toucan.db :as db])
  (:import java.sql.Timestamp))

(defn sql-count
  "Wrap a query in count."
  [sub-query]
  {:select [:%count.*] :from [[sub-query :c]]})

(defn results
  "Run your query adding in next previous and counts"
  ([view] (results view 30 1))
  ([view page-size] (results view page-size 1))
  ([view page-size page]
   (let [count (get (first (db/query (sql-count view))) :count 0)
         per-page (or page-size 30)
         current-page (* per-page (- (or page 1) 1))
         pages (int (Math/ceil (/ count per-page)))]
     {:count count
      :next (when (< current-page pages) (+ current-page 1))
      :previous (when (> current-page 0) (- current-page 1))
      :results (db/query
                (-> view
                    (sqlh/limit per-page)
                    (sqlh/offset current-page)))})))

