(ns django-clj.auth
  (:require [toucan.db :as db]
            [buddy.sign.jwt :as jwt]
            [django-clj.users :refer [django-user-login django-user-by-id]])
  (:import
   (de.mkammerer.argon2
    Argon2Factory
    Argon2Factory$Argon2Types)))

(defonce argon2 (Argon2Factory/create))
(defonce argon2i (Argon2Factory/create Argon2Factory$Argon2Types/ARGON2i))
(defonce argon2id (Argon2Factory/create Argon2Factory$Argon2Types/ARGON2id))
(defonce ^:private ^:const arg2-iterations 2)
(defonce ^:private ^:const arg2-memory 512)
(defonce ^:private ^:const arg2-parallelism 1)

(defn remove-bearer
  "Little helper to remove the word Bearer"
  [header]
  (clojure.string/replace header #"Bearer " ""))

(defn- argon2-hash
  "Hash string with Argon2id"
  [str-to-be-hashed]
  (.hash argon2id arg2-iterations arg2-memory arg2-parallelism str-to-be-hashed))

(defn- argon2-verify
  "verify argon2-hash(plaintext_str) == hashed_str"
  ([hashed-str plaintext-str] (argon2-verify hashed-str plaintext-str nil))
  ([hashed-str plaintext-str argon-type]
   (case argon-type
     "argon2i" (.verify argon2i hashed-str plaintext-str)
     (.verify argon2id hashed-str plaintext-str))))

(defn django-verify-password [password-hash plain-password]
  (when password-hash
    (let [[hash-type & hash-parts] (clojure.string/split password-hash #"\$")]
      (when (= hash-type "argon2")
        (argon2-verify (str "$" (clojure.string/join "$" hash-parts)) plain-password (first hash-parts))))))

(defn django-create-password
  ([plain-password] (django-create-password plain-password "argon2"))
  ([plain-password hash-type]
   (when (= hash-type "argon2")
     (str hash-type (argon2-hash plain-password)))))

(defn django-user-auth [username password]
  (let [user (-> (django-user-login username)
                 (db/query)
                 first)]
    (when (django-verify-password (:password user) password)
      user)))

(defn django-jwt-sign [secret {:keys [id is_staff is_superuser] :or {is_staff false is_superuser false}}]
  (when (and secret id)
    (jwt/sign
     {:token_type "access"
      :exp (-> (java.time.LocalDateTime/now) (.plusHours 8) (.toString))
      :user_id id
      :is_staff is_staff
      :is_superuser is_superuser}
     secret
     {:alg :hs256})))

;;DEPRECATE
#_(defn django-sign-user [secret username password]
  (when-let [user (django-user-auth username password)]
    (jwt/sign
     {:token_type "access"
      :exp (-> (java.time.LocalDateTime/now) (.plusHours 8) (.toString))
      :user_id (:id user)
      :is_staff (:is_staff user)
      :is_superuser (:is_superuser user)}
     secret
     {:alg :hs256 :skip-validation true})))

(defn django-generate-user-token [secret username password]
  (when-let [user (django-user-auth username password)]
    (django-jwt-sign
     secret
     {:id (:id user)
      :is_staff (:is_staff user)
      :is_superuser (:is_superuser user)})))

(defn django-jwt-unsign [secret data]
  (when (and secret data)
    (try
      (jwt/unsign
       data
       secret
       {:alg :hs256 :skip-validation true})
      (catch  clojure.lang.ExceptionInfo e nil))))

(defn django-user-by-jwt [secret data]
  (-> (django-jwt-unsign secret (remove-bearer data))
      :user_id
      django-user-by-id))

