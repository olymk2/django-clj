(ns django-clj.tables
  (:require [honeysql.helpers :as sqlh]))

(def django-user-table :auth_user)
(def django-user-user-permissions-table :auth_user_user_permissions)
(def django-group-table :auth_group)
(def django-user-group-table :auth_user_groups)
(def django-group-permissions-table :auth_groups_permissions)
(def django-permission-table :auth_user_user_permissions)


;; Default django table on a new setup
;; need to be defn because the user table models names can change
(defn django-auth-user [] (-> (sqlh/select :*) (sqlh/from django-user-table)))
(defn django-auth-user-user-permissions [] (-> (sqlh/select :*) (sqlh/from django-user-user-permissions-table)))
(defn django-auth-user-groups [] (-> (sqlh/select :*) (sqlh/from django-user-group-table)))
(defn django-auth-group [] (-> (sqlh/select :*) (sqlh/from django-group-table)))
(defn django-auth-group-permissions [] (-> (sqlh/select :*) (sqlh/from django-group-permissions-table)))
(defn django-auth-permission [] (-> (sqlh/select :*) (sqlh/from django-permission-table)))


(def django-admin-log (-> (sqlh/select :*) (sqlh/from :django_admin_log)))
(def django-content-type (-> (sqlh/select :*) (sqlh/from :django_content_type)))
(def django-migrations (-> (sqlh/select :*) (sqlh/from :django_migrations)))
(def django-session (-> (sqlh/select :*) (sqlh/from :django_session)))

