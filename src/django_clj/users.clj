(ns django-clj.users
  (:require
   [toucan.db :as db]
   [honeysql.helpers :as sqlh]
   [django-clj.tables :refer
    [django-user-table
     django-user-user-permissions-table
     django-group-table
     django-user-group-table
     django-group-permissions-table
     django-permission-table]]
   [toucan.models :refer [defmodel hydration-keys IModel]]))

(defn insert-user
  "Add a new user to the django user table."
  [{:keys [username first_name last_name email is_active password is_superuser is_staff]
    :or {is_superuser false is_staff false is_active true}}]
  (db/execute! (-> (sqlh/insert-into django-user-table)
                   (sqlh/values [{:username username
                                  :first_name first_name
                                  :last_name last_name
                                  :email email
                                  :is_active is_active
                                  :password password
                                  :is_superuser is_superuser
                                  :is_staff is_staff}]))))

#_(defn create-user
    "Add a new user to the django user table, returning the new user."
    [user]
    (let [create (handle-db-exceptions
                  #(insert-user
                    (update
                     user
                     :password
                     auth/django-create-password)))]
      (if (= create '(1))
        {:status 200 :body (dissoc (first (db/query (django-user (:username user)))) :password)}
        {:status (:status create) :body "Failed to create user"})))

(defn django-user-login [username]
  (-> (sqlh/select :*)
      (sqlh/from [django-user-table :user])
      (sqlh/where [:= :user.username username])))

(defn django-users
  ([] (django-users django-user-table))
  ([user-table]
   (-> (sqlh/select :user.id
                    :user.username
                    :user.email
                    :user.is_staff
                    :user.is_superuser
                    [:%json_agg.user_perms.* :user_perms]
                    [:%json_agg.group.* :user_groups])
       (sqlh/from [user-table :user])
       (sqlh/left-join [django-user-group-table :user_group] [:= :user.id :user_group.user_id]
                       [:auth_group :group] [:= :user_group.group_id :group.id]
                       [django-user-user-permissions-table :user_perms] [:= :user_perms.user_id :user.id]
                       [:auth_permission :perm] [:= :user_perms.permission_id :perm.id])
       (sqlh/group :user.id))))

(def django-user-tokens
  (-> (sqlh/select :token.user_id)
      (sqlh/from [:authtoken_token :token])))

(defn django-token
  "Find a user from django database structure, return anonymous user if not matched."
  [token]
  (first (db/query
          (-> django-user-tokens
              (sqlh/merge-where [:= :token.key token])))))

(defn django-user [username]
  (-> (sqlh/select :*)
      (sqlh/from [django-user-table :user])
      (sqlh/where [:= :user.username username])))

(defn django-user-by
  "Find a user from django database structure, return anonymous user if not matched."
  [user]
  (or (first
       (db/query
        (-> (django-users)
            (sqlh/merge-where
             (when-let [user-id (:user_id user)]
               [:= :user.id  user-id]))

            (sqlh/merge-where
             (when-let [username (:username user)]
               [:= :user.username  username]))

            (sqlh/merge-where
             (when-let [user-email (:user_email user)]
               [:= :user.email  user-email])))))
      {:is_anon true}))

(defn django-user-by-id [id]  (when (int? id) (django-user-by {:user_id id})))
(defn django-user-by-username [username] (django-user-by {:username username}))
(defn django-user-by-email [email] (django-user-by {:user_email email}))
(defn django-user-by-token [token]  (when-not (clojure.string/blank? token)
                                      (-> (django-token token)
                                          :user_id
                                          django-user-by-id)))

