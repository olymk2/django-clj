(ns django-clj.ring.middleware
  (:require
   [django-clj.auth :refer [django-user-by-jwt django-jwt-unsign]]))

(defn django-user-is-staff [secret data]
  (true? (:is_staff (django-user-by-jwt secret data))))

(defn django-user-is-superuser [secret data]
  (true? (:is_superuser (django-user-by-jwt secret data))))

(defn auth-token-required
  "This function checks fora a valid auth token"
  [handler]
  (fn [request]
    (let [auth (django-jwt-unsign "" (-> request :parameters :header :authorization))]
      (if (nil? auth)
        (handler request)
        (handler (assoc request :identity auth))))))


(defn auth-staff-token
  "This function checks fora a valid auth token"
  [handler]
  (fn [request]
    (let [auth (django-user-is-staff
                (:jwt-secret-key config)
                (-> request :parameters :header :authorization remove-bearer))]
      (if (nil? auth)
        (handler request)
        (handler (assoc request :identity auth))))))

(comment
  ((auth-token-required {:parameters {:header {:authorization "123"}}}) {})
  (django-jwt-unsign "jgjg" "123")
  )
