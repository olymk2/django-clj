(ns django-clj.specs)

;; Limit seems to be 9 by default
(s/def :django/password (s/and string? #(> (count %) 8)))
(s/def :django/count nat-int?)
(s/def :django/next (s/nilable nat-int?))
(s/def :django/previous (s/nilable nat-int?))
(s/def :django/page (s/nilable pos-int?))
(s/def :django/page-size (s/nilable pos-int?))

(comment
  (s/conform :django/previous 0)
  (s/conform :django/previous "0")
  (s/valid? :django/previous 0)

  (gen/sample (s/gen :django/count) 10)

  (gen/generate (s/gen :django/count))
  (gen/generate (s/gen :django/next))
  (gen/generate (s/gen :django/previous)))
